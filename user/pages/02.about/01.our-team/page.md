---
title: 'Our Team'
page_header_size: lg
paragraphs:
    -
        text: '# Our Team'
        side: left
        size: md
    -
        text: "## Kaye McNeill\r\n### Principal Physiotherapist\r\nKaye is well known for excellence in diagnosis, treatment of musculoskeletal sports and spinal injuries and patient care.\r\n\r\nKayes treatment relies heavily on excellent experience in manual therapy techniques, including MAITLAND, MCKENZIE, MULLIGAN TECHNIQUES, correct exercise prescription, encouragement and relationship building."
        side: left
        size: md
        image:
            user/pages/02.about/01.our-team/IMG_0016.JPG:
                name: IMG_0016.JPG
                type: image/jpeg
                size: 209596
                path: user/pages/02.about/01.our-team/IMG_0016.JPG
    -
        text: "## Jeff McNeill\r\n### Practice Manager\r\nJeff has had over 30 years’ experience in managing multidisciplinary physiotherapy practices in Melbourne and Bairnsdale.\r\n\r\nJeff has a background in social welfare and held a position of East Gippsland Shire Councillor between 2012 – 2016.\r\n\r\nJeff is a member of the Victorian Disability Advisory Council to Martin Foley, Minister of Ageing, Housing and Disability."
        side: left
        size: md
        image:
            user/pages/02.about/01.our-team/IMGP2119.JPG:
                name: IMGP2119.JPG
                type: image/jpeg
                size: 522189
                path: user/pages/02.about/01.our-team/IMGP2119.JPG
---

