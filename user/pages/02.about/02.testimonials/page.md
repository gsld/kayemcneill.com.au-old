---
title: Testimonials
page_header_size: md
paragraphs:
    -
        text: "# Testimonials\r\n\r\n> Hi Kaye, just wanting to say thanks I saw you a couple of weeks ago with a really bad back and you were excellent my back is as good as gold now - **Terry Nippers**"
        side: left
        size: md
visible: true
---

# Testimonials
