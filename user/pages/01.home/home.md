---
title: Home
page_header_size: lg
page_header:
    -
        image:
            user/pages/01.home/hero10.jpg:
                name: hero10.jpg
                type: image/jpeg
                size: 229605
                path: user/pages/01.home/hero10.jpg
    -
        image:
            user/pages/01.home/newhero1020.jpg:
                name: newhero1020.jpg
                type: image/jpeg
                size: 33494
                path: user/pages/01.home/newhero1020.jpg
    -
        image:
            user/pages/01.home/hero20.jpg:
                name: hero20.jpg
                type: image/jpeg
                size: 278065
                path: user/pages/01.home/hero20.jpg
intro: '**''Bairnsdale Spinal Sports & Manipulative Therapy''** is the private practice of Kaye McNeill. Kaye is a Musculoskeletal physiotherapist with over 30 years clinical experience in private practice. Kaye moved to Bairnsdale in 1994 for a lifestyle change after being principal physiotherapist of Canterbury and Box Hill physiotherapy clinics for 13 years.'
one: "## Daily Emergency Appointments Reserved. \r\n## Call us today - no referral necessary."
two:
    -
        text: "### About Kaye\r\nKaye's specialty is in spinal, sports and work related injuries and she is an Australian Physiotherapy Association Member and she is a registered provider with Medicare Australia, Department of Veterans Affairs, WorkCover and TAC.\r\n\r\nKaye graduated from Lincoln Institute (Melbourne University) with a degree in physiotherapy and later trained for two years to gain post graduate diploma in Manipulative Therapy."
        size: md
        image:
            user/pages/01.home/IMG_0016.JPG:
                name: IMG_0016.JPG
                type: image/jpeg
                size: 209596
                path: user/pages/01.home/IMG_0016.JPG
    -
        text: "Kaye is committed to a high professional  standard and excellent communication with her clients to gain the best results. She draws on a wide variety of treatments including joint mobilization, MAITLAND, MCKENZIE, MULLIGAN TECHNIQUES, soft tissue massage, rehabilitation exercise, taping techniques and compliments this with education and advice. \r\n\r\n**Kaye also offers Dry Needling Therapy and is a Certified Dry Needling Practitioner.**"
        side: left
        size: md
        image:
            user/pages/01.home/20161023_150014.jpg:
                name: 20161023_150014.jpg
                type: image/jpeg
                size: 307799
                path: user/pages/01.home/20161023_150014.jpg
metadata:
    description: ''
    keywords: ''
    author: 'East Gippsland Design'
body_classes: page-home
published: true
menu: Home
slug: home
process:
    twig: true
    markdown: false
visible: true
template: home
---
