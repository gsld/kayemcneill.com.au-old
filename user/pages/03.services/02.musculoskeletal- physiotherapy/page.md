---
title: 'Musculoskeletal Physiotherapy'
page_header_size: lg
paragraphs:
    -
        text: "# Musculoskeletal Physiotherapy\r\n\r\nKaye provides ‘hands on’ physiotherapy and is highly skilled in clinical examination.\r\n\r\nMusculoskeletal Physiotherapy is the assessment and management of injuries pertaining to joint, muscle, ligament & tendons and nervous system.\r\n\r\nKaye is highly skilled in mobility & manipulation of these areas and focuses on a skilled diagnosis."
        side: left
        size: md
gallery:
    'user/pages/03.services/02.musculoskeletal- physiotherapy/header0.jpg':
        name: header0.jpg
        type: image/jpeg
        size: 86092
        path: 'user/pages/03.services/02.musculoskeletal- physiotherapy/header0.jpg'
    'user/pages/03.services/02.musculoskeletal- physiotherapy/0.jpeg':
        name: 0.jpeg
        type: image/jpeg
        size: 142912
        path: 'user/pages/03.services/02.musculoskeletal- physiotherapy/0.jpeg'
---

