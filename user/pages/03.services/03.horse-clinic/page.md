---
title: 'Horse Clinic'
page_header_size: md
paragraphs:
    -
        text: "# Horse Clinic\r\n\r\nThe role of Horse Clinics is to provide riders with sufficient core stability and strength to maintain good posture and trunk stability in the saddle. Reduced flexibility, tight muscles, restricted joints, or simply nervous tension in the rider can all reduce controlled stability by inhibiting the ‘core muscles’ and thus the ability to adapt to the movements of your horse.\r\n\r\n**Prospective Injuries from Poor Core Stability:**\r\n- Lower back pain (lumbar spine and/or sacroiliac joint)\r\n- Abdominal Strains \r\n- Groin Strains\r\n- Hip Flexor / Abductor / Adductor Strains\r\n- Pelvic Misalignment\r\n- Other Musculoskeletal Injuries due to compensation"
        side: left
        size: md
gallery:
    user/pages/03.services/02.horse-clinic/409000_10150607262773592_64701137_n.jpg:
        name: 409000_10150607262773592_64701137_n.jpg
        type: image/jpeg
        size: 67069
        path: user/pages/03.services/02.horse-clinic/409000_10150607262773592_64701137_n.jpg
    user/pages/03.services/02.horse-clinic/307716_2026476696853_1221855305_n.jpg:
        name: 307716_2026476696853_1221855305_n.jpg
        type: image/jpeg
        size: 78238
        path: user/pages/03.services/02.horse-clinic/307716_2026476696853_1221855305_n.jpg
    user/pages/03.services/02.horse-clinic/429377_10150763657627028_714261887_n.jpg:
        name: 429377_10150763657627028_714261887_n.jpg
        type: image/jpeg
        size: 55157
        path: user/pages/03.services/02.horse-clinic/429377_10150763657627028_714261887_n.jpg
    user/pages/03.services/02.horse-clinic/1888526_10202673671258240_8367719818510490260_n.jpg:
        name: 1888526_10202673671258240_8367719818510490260_n.jpg
        type: image/jpeg
        size: 104377
        path: user/pages/03.services/02.horse-clinic/1888526_10202673671258240_8367719818510490260_n.jpg
    user/pages/03.services/02.horse-clinic/11879025_10204994001265040_2789562777067515407_o.jpg:
        name: 11879025_10204994001265040_2789562777067515407_o.jpg
        type: image/jpeg
        size: 74761
        path: user/pages/03.services/02.horse-clinic/11879025_10204994001265040_2789562777067515407_o.jpg
    user/pages/03.services/02.horse-clinic/11893957_10204994001465045_6908932918698788769_o.jpg:
        name: 11893957_10204994001465045_6908932918698788769_o.jpg
        type: image/jpeg
        size: 63823
        path: user/pages/03.services/02.horse-clinic/11893957_10204994001465045_6908932918698788769_o.jpg
---

