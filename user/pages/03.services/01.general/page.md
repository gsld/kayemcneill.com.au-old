---
title: 'General Services'
page_header_size: md
paragraphs:
    -
        text: "# Services\r\n## Return to Work Assistance and Planning\r\nWe negotiate with your employer and doctor as part of a team approach to injury recovery.\r\n\r\n**We aim to get you back to work as soon as possible.**\r\n\r\n## TAC Rehabilitation\r\nWe have a great relationship with the TAC because our passion is patient care and helping clients return to their best after road trauma.\r\n\r\nWe treat very serious road trauma patients to minor sprains in all accidents.\r\n\r\n**Kaye is Early Intervention trained to assist the TAC with speedy recovery.**\r\n\r\n## Hair Dresser Clinics\r\nTight neck and shoulders, headaches and poor posture are often found in the hair dressing industry.\r\n\r\n**We teach hair dressers how to manage their posture and maintain their longevity in the industry.**\r\n\r\n## Ski Clinics\r\nCome and be informed of pre-season ski fitness and strength & flexibility training.\r\n\r\n## Sport\r\nKaye is a musculoskeletal specialist and has had many years’ experience treating, managing and advising athletes and recreationalists.\r\n\r\n## Industry\r\nOur clinic has the skills to devise programs for industries to create optimum safety and a healthier workforce.\r\n**Kaye conducts musculoskeletal screening for assessments for large industry sectors.**"
        side: left
        size: md
visible: true
---

