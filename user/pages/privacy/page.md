---
title: Privacy
---

# Privacy
## Privacy Policy / Disclaimer
This website disclaims all liability to any person in respect of anything, and of the consequences of anything, done or omitted to be done by any such person in reliance, whether wholly or partially, upon any information presented on this site.
If you require any more information or have any questions about our privacy policy, please feel free to contact us by email at the email address found on our contact page. At This site the privacy of our visitors is of extreme importance to us. This privacy policy document outlines the types of personal information that is received and collected by this website and how it is used.

## Log Files Policy
Like most websites, this site makes use of log files. The information inside the log files includes internet protocol (IP) addresses, type of browser, Internet Service Provider (ISP), date/time stamp, referring/exit pages, and number of clicks to analyze trends, administer the site, track user’s movement around the site, and gather demographic information. IP addresses, and other such information are not linked to any information that is personally identifiable.

## Cookies Policy
**What are cookies?**
Cookies are small text files that are placed on your computer by websites that you visit. These text files can be read by these websites and help to identify you when you return to a website. Cookies can be “persistent” or “session” cookies. Persistent cookies remain on your computer when you have gone offline, while session cookies are deleted as soon as you close your web browser.

To find out more about cookies, including how to see what cookies have been set and how to block and delete cookies, please visit http://www.aboutcookies.org/.

### Cookies on this Website
In common with most other websites, we use cookies and similar technologies to help us understand how people use this website so that we can keep improving our services. 

If you choose to use this website without blocking or disabling cookies or opting out of other technologies, you will indicate your consent to our use of these cookies and other technologies and to our use (in accordance with this privacy policy) of any personal information that we collect using these technologies. If you do not consent to the use of these technologies, please be sure to block or disable them using your browser settings.
