---
title: Sitemap
slug: sitemap
menu: Sitemap
published: true
visible: false
template: sitemap
process:
  markdown: false
  twig: true
---

{% macro loop(page) %}
    {% for p in page.children.visible %}
        {% set current_page = (p.active or p.activeChild) ? 'active' : '' %}
       {% if p.children.visible.count > 0 %}
        <li class="dropdown {{ current_page }}">
        {% else %}
        <li class="{{ current_page }}">
        {% endif %}
            {% if p.children.visible.count > 0 %}
            <a class="nav-parent dropdown-toggle" data-toggle="dropdown">{{ p.menu }} <i class="fa fa-arrow-circle-down"></i></a>
            {% else %}
            <a class="" href="{{ p.url }}">{{ p.menu }}</a>
            {% endif %}
            {% if p.children.visible.count > 0 %}
                <ul class="dropdown-menu">
                <li><a href="{{ p.url }} ">{{ p.title }}</a></li>
                    {{ _self.loop(p) }}
                </ul>
            {% endif %}
        </li>
    {% endfor %}
{% endmacro %}

  <ul class="nav navbar-nav">
        {{ _self.loop(pages) }}
    {% for mitem in site.menu %}
        <li class="">
            <a href="{{ mitem.url }}">
                {{ mitem.text }}
            </a>
        </li>
    {% endfor %}
</ul>
