---
title: Styleguide
slug: styleguide
menu: Styleguide
published: true
visible: false
template: page
body_classes: page-colophon
process:
  markdown: false
  twig: true
metadata:
  description: description
  keywords: keywords
  author: author
---


<h1 class="page-heading">Styleguide</h1>
<div class="page-content colophon container">
  <div class="site-styleguide">

      <section >
        <h3 class="section-heading">Colours</h3>
        <br>
        <div class="colour-1">
          <strong class="inner-heading">Colour 1</strong>
          <span class="block x100">100%</span>
          <span class="block x70">70%</span>
          <span class="block x50">50%</span>
          <span class="block x30">30%</span>
          <span class="block x20">20%</span>
          <span class="block x10">10%</span>
          <span class="block x5">5%</span>
          <br><br>
          <p>Colour 1 at 100%</p>
          <p class="text70">Colour 1 at 70%</p>
          <p class="text30">Colour 1 at 30%</p>
          <p class="text10">Colour 1 at 10%</p>
        </div>
        <br><br>

        <div class="colour-2">
          <strong class="inner-heading">Colour 2</strong>
          <span class="block x100">100%</span>
          <span class="block x70">70%</span>
          <span class="block x50">50%</span>
          <span class="block x30">30%</span>
          <span class="block x20">20%</span>
          <span class="block x10">10%</span>
          <span class="block x5">5%</span>
          <br><br>
          <p>Colour 2 at 100%</p>
          <p class="text70">Colour 2 at 70%</p>
          <p class="text30">Colour 2 at 30%</p>
          <p class="text10">Colour 2 at 10%</p>
        </div>

        <div class="colour-3">
          <strong class="inner-heading">Colour 3</strong>
          <span class="block x100">100%</span>
          <span class="block x70">70%</span>
          <span class="block x50">50%</span>
          <span class="block x30">30%</span>
          <span class="block x20">20%</span>
          <span class="block x10">10%</span>
          <span class="block x5">5%</span>
          <br><br>
          <p>Colour 3 at 100%</p>
          <p class="text70">Colour 3 at 70%</p>
          <p class="text30">Colour 3 at 30%</p>
          <p class="text10">Colour 3 at 10%</p>
        </div>

        <div class="colour-4">
          <strong class="inner-heading">Colour 4</strong>
          <span class="block x100">100%</span>
          <span class="block x70">70%</span>
          <span class="block x50">50%</span>
          <span class="block x30">30%</span>
          <span class="block x20">20%</span>
          <span class="block x10">10%</span>
          <span class="block x5">5%</span>
          <br><br>
          <p>Colour 4 at 100%</p>
          <p class="text70">Colour 4 at 70%</p>
          <p class="text30">Colour 4 at 30%</p>
          <p class="text10">Colour 4 at 10%</p>
        </div>


      </section>

        <br><br>

      <section>
        <h3 class="section-heading">Paragraph Styles</h3>
        <h1>Heading 1</h1>
        <h2>Heading 2</h2>
        <h3>Heading 3</h3>
        <h4>Heading 4</h4>
        <h5>Heading 5</h5>
        <h6>Heading</h6>
        <hr>
        <p>Paragraph text. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta amet enim inventore expedita et sunt illum accusamus quidem nobis numquam voluptatum alias itaque laboriosam. Quam ipsam dolores sint ducimus aspernatur!</p>
        <p>Harum adipisci ex eos nisi explicabo eaque dolores aut optio accusamus dolore cumque dignissimos fugiat accusantium consequuntur minima.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias sapiente suscipit velit mollitia minima dolores quasi architecto repellat dicta nulla excepturi magnam nisi id ea iste assumenda fugit deleniti omnis.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore cupiditate est reiciendis non similique veritatis eaque quidem architecto voluptas aut! Doloremque doloribus molestiae quae explicabo aut sequi totam praesentium excepturi!</p>
        <hr>
       <div class="font-resize-test">
         <strong>Font Size Scale</strong>
         <br><br>
          <p class="font-size-xs">Font size X Small</p>
          <p class="font-size-sm">Font size Small</p>
          <p class="">Font size Normal</p>
          <p class="font-size-md">Font size Medium</p>
          <p class="font-size-lg">Font size Large</p>
          <p class="font-size-xlg">Font size X Large</p>
          <p class="font-size-xxlg">Font size XX Large</p>
        </div>
        <hr>
        <p><strong>Bold text.</strong></p>
        <p><em>Italic text.</em></p>
        <hr>
        <div class="page-user-content">
          <ul>
            <li>Unordered lists</li>
            <li>With list items
              <ul>
                <li>List items within list items</li>
                <li>And more</li>
              </ul>
            </li>
            <li>And more items</li>
          </ul>
          <ol>
            <li>Ordered lists</li>
            <li>With more list items
              <ol>
                <li>List items within list items</li>
                <li>And more</li>
              </ol>
            </li>
            <li>Some latin?</li>
            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex reiciendis alias libero officia quos velit laborum aliquid inventore ea tempore a dolor rerum quam sapiente autem dolorem officiis quisquam nulla.</li>
          </ol>
       </div>
      </section>

        <br><br>

      <section>
        <h3 class="section-heading">Buttons</h3>
        <br><br>
        <div>
          <a href="#" class="btn btn-one">Button One</a>
        </div>
        <br><br>
        <div>
          <a href="#" class="btn btn-action">Button Action</a>
        </div>
        <br>
      </section>


  </div>
    <div class="site-engineering">
      <br><br><br><br>
      <section>
        <h3 class="section-heading">Colophon</h3>
      <h1>Colophon</h1>
    <p> (interesting information about how this site is built) </p>
    <h2>Who Built This Site?</h2>
    <p><a href="http://{{ site.author.web }}">{{ site.author.name }}</a> - the professional design solution for East Gippsland</p>

    <h2>What Tools Were Used?</h2>
    <p>Html5, CSS3, Responsive Web Design (RWD), <a href="http://getbootstrap.com">Bootstrap</a>, <a href="https://typekit.com/">Typekit</a>, <a target="_blank" href="http://fortawesome.github.com/Font-Awesome">Font Awesome</a>, <a href="http://www.sublimetext.com/">Sublime Text</a>, Fireworks, a 27" iMac, and exceptional home roasted coffee thanks to a <a href="http://beanbay.coffeesnobs.com.au/">Behmor 1600 from CoffeeSnobs</a></p>
</section>
  </div>

</div>
