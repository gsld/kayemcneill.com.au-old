---
title: Contact
page_heading: Contact
page_header_size: sm
paragraphs:
    -
        text: "# We reserve emergency appointments daily.\r\n## Contact information\r\n\r\n### Address:\r\n30 Louise Drv, Lucknow VIC 3875\r\n\r\n### Phone: \r\n(03) 5152 6088\r\n\r\n### Fax: \r\n(03) 5153 0022\r\n\r\n### Email:\r\nadmin@kayemcneill.com.au"
        size: md
    -
        size: md
        image:
            user/pages/05.contact/IMG13200.jpg:
                name: IMG13200.jpg
                type: image/jpeg
                size: 42706
                path: user/pages/05.contact/IMG13200.jpg
process:
    markdown: true
    twig: true
cache_enable: false
map_image:
    'user/pages/04.contact/Screen Shot 2017-01-12 at 12.48.36 pm.png':
        name: 'Screen Shot 2017-01-12 at 12.48.36 pm.png'
        type: image/png
        size: 175920
        path: 'user/pages/04.contact/Screen Shot 2017-01-12 at 12.48.36 pm.png'
---
