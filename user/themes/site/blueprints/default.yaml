title: Standard Page

rules:
  slug:
    pattern: "[a-zа-я][a-zа-я0-9_\-]+"
    min: 2
    max: 80

form:
  validation: loose

  fields:

    tabs:
      type: tabs
      active: 1

      fields:
        content:
          type: tab
          title: PLUGIN_ADMIN.CONTENT

          fields:
            header.title:
              type: text
              autofocus: true
              style: vertical
              label: PLUGIN_ADMIN.TITLE

            folder:
              type: text
              label: Page Slug (url)
              style: vertical
              validate:
                type: slug

            name:
              type: select
              classes: fancy
              label: Page Type (template)
              help: Select the page type
              style: vertical
              default: page
              data-options@: '\Grav\Common\Page\Pages::pageTypes'

            header.page_heading:
              type: text
              style: vertical
              label: Page Heading

            section_header:
              type: section
              title: Header
              underline: true

            page_header_instructions:
              type: display
              label:
              style: vertical
              markdown: false
              content: "<strong style='font-size:21px;'>Page Header or Slider</strong> <br> Select a size then add an image. To make this a slider, add multiple rows."

            header.page_header_size:
              type: toggle
              label: Image / Slider Size
              style: vertical
              highlight: sm
              default: sm
              options:
                sm: Small
                md: Medium
                lg: Large

            header.page_header:
              name: page_header
              type: list
              style: vertical
              label: ""
              fields:
                .image:
                  type: file
                  style: vertical
                  label: Add Image
                  destination: '@self'
                  multiple: false
                  limit: 10
                  filesize: 100
                  accept:
                    - image/*
                .text:
                  type: textarea
                  label: Image or Slider Text
                  style: vertical
                  validate:
                    type: textarea

            section_content:
              type: section
              title: Content
              underline: true

            header.paragraphs:
              name: paragraphs
              type: list
              style: vertical
              label: Paragraphs
              fields:

                .text:
                  type: markdown
                  label: Paragraph Text
                  validate:
                    type: textarea

                .side:
                  type: toggle
                  label: Image Side
                  style: vertical
                  options:
                    left: Left
                    right: Right

                .size:
                  type: toggle
                  label: Image Size
                  style: vertical
                  highlight: md
                  default: md
                  options:
                    sm: Small
                    md: Medium
                    lg: Large

                .image:
                  type: file
                  style: vertical
                  label: Paragraph Images
                  destination: '@self'
                  multiple: true
                  limit: 10
                  filesize: 100
                  accept:
                    - image/*

            section_images:
              type: section
              title: Gallery, Downloads and Links
              underline: true

            header.gallery:
              type: file
              style: vertical
              label: Page Gallery
              destination: '@self'
              multiple: true
              limit: 50
              filesize: 100
              accept:
                - image/*

            header.downloads:
              name: downloads
              type: list
              style: vertical
              label: Page Downloads
              fields:
                .text:
                  type: text
                  label: Download Button Text
                  style: vertical
                .dl:
                  type: file
                  label: Add a file
                  accept: ["image/*", '.doc', '.docx', '.xls', '.xlsx', '.pdf']
                  style: vertical
                  destination: '@self'
                  multiple: false
                  limit: 1
                  filesize: 50

            header.links:
              name: links
              type: list
              style: vertical
              label: Page Links
              fields:
                .text:
                  type: text
                  label: Link Button Text
                  style: vertical
                .link:
                  type: text
                  label: "Link URL (format must be 'http://' for external links and '/link' for internal links"
                  style: vertical

            section_page_nav:
              type: section
              title: Page Nav
              underline: true

            header.page_nav_text:
              name: page_nav_text
              type: list
              style: vertical
              label: Page Nav Text
              fields:
                .text:
                  type: markdown
                  style: vertical
                  label:
                  validate:
                    type: textarea

            header.page_nav:
              name: page_nav
              type: list
              style: vertical
              label: Page Nav Links
              fields:
                .text:
                  type: text
                  label: Link Text
                  style: vertical
                .link:
                  type: text
                  label: "Link URL (format must be 'http://' for external links, '/link' for internal links and '#link' for in-page links."
                  style: vertical

        seo:
          type: tab
          title: SEO

          fields:

            header.metadata:
              toggleable: true
              type: array
              label: PLUGIN_ADMIN.METADATA
              help: PLUGIN_ADMIN.METADATA_HELP
              placeholder_key: PLUGIN_ADMIN.METADATA_KEY
              placeholder_value: PLUGIN_ADMIN.METADATA_VALUE

        options:
          type: tab
          title: PLUGIN_ADMIN.OPTIONS

          fields:

            columns:
              type: columns
              fields:
                column1:
                  type: column
                  fields:

                    settings:
                      type: section
                      title: PLUGIN_ADMIN.SETTINGS
                      underline: true

                    ordering:
                      type: toggle
                      label: PLUGIN_ADMIN.FOLDER_NUMERIC_PREFIX
                      help: PLUGIN_ADMIN.FOLDER_NUMERIC_PREFIX_HELP
                      highlight: 1
                      options:
                        1: PLUGIN_ADMIN.ENABLED
                        0: PLUGIN_ADMIN.DISABLED
                      validate:
                        type: bool

                    route:
                      type: select
                      label: PLUGIN_ADMIN.PARENT
                      classes: fancy
                      data-options@: '\Grav\Common\Page\Pages::parentsRawRoutes'
                      data-default@: '\Grav\Plugin\admin::rawRoute'
                      options:
                        '/': PLUGIN_ADMIN.DEFAULT_OPTION_ROOT

                    header.body_classes:
                      type: text
                      label: PLUGIN_ADMIN.BODY_CLASSES


                column2:
                  type: column

                  fields:
                    order_title:
                      type: section
                      title: PLUGIN_ADMIN.ORDERING
                      underline: true

                    order:
                      type: order
                      label: PLUGIN_ADMIN.PAGE_ORDER
                      sitemap:

            publishing:
              type: section
              title: PLUGIN_ADMIN.PUBLISHING
              underline: true

              fields:
                header.published:
                  type: toggle
                  toggleable: true
                  label: PLUGIN_ADMIN.PUBLISHED
                  help: PLUGIN_ADMIN.PUBLISHED_HELP
                  highlight: 1
                  size: medium
                  options:
                    1: PLUGIN_ADMIN.YES
                    0: PLUGIN_ADMIN.NO
                  validate:
                    type: bool

                header.publish_date:
                  type: datetime
                  label: PLUGIN_ADMIN.PUBLISHED_DATE
                  toggleable: true
                  help: PLUGIN_ADMIN.PUBLISHED_DATE_HELP

                header.unpublish_date:
                  type: datetime
                  label: PLUGIN_ADMIN.UNPUBLISHED_DATE
                  toggleable: true
                  help: PLUGIN_ADMIN.UNPUBLISHED_DATE_HELP

        advanced:
          type: tab
          title: PLUGIN_ADMIN.ADVANCED

          fields:

            overrides:
              type: section
              title: PLUGIN_ADMIN.OVERRIDES
              underline: true

              fields:

                header.dateformat:
                  toggleable: true
                  type: select
                  size: medium
                  selectize:
                    create: true
                  label: PLUGIN_ADMIN.DEFAULT_DATE_FORMAT
                  help: PLUGIN_ADMIN.DEFAULT_DATE_FORMAT_HELP
                  placeholder: PLUGIN_ADMIN.DEFAULT_DATE_FORMAT_PLACEHOLDER
                  data-options@: '\Grav\Common\Utils::dateFormats'
                  validate:
                    type: string

                header.menu:
                  type: text
                  label: PLUGIN_ADMIN.MENU
                  toggleable: true
                  help: PLUGIN_ADMIN.MENU_HELP

                header.slug:
                  type: text
                  label: PLUGIN_ADMIN.SLUG
                  toggleable: true
                  help: PLUGIN_ADMIN.SLUG_HELP
                  validate:
                    message: PLUGIN_ADMIN.SLUG_VALIDATE_MESSAGE
                    rule: slug

                header.redirect:
                  type: text
                  label: PLUGIN_ADMIN.REDIRECT
                  toggleable: true
                  help: PLUGIN_ADMIN.REDIRECT_HELP

                header.process:
                  type: checkboxes
                  label: PLUGIN_ADMIN.PROCESS
                  toggleable: true
                  config-default@: system.pages.process
                  default:
                    markdown: true
                    twig: false
                  options:
                    markdown: Markdown
                    twig: Twig
                  use: keys

                header.child_type:
                  type: select
                  toggleable: true
                  label: PLUGIN_ADMIN.DEFAULT_CHILD_TYPE
                  default: default
                  placeholder: PLUGIN_ADMIN.USE_GLOBAL
                  data-options@: '\Grav\Common\Page\Pages::types'

                header.routable:
                  type: toggle
                  toggleable: true
                  label: PLUGIN_ADMIN.ROUTABLE
                  help: PLUGIN_ADMIN.ROUTABLE_HELP
                  highlight: 1
                  options:
                    1: PLUGIN_ADMIN.ENABLED
                    0: PLUGIN_ADMIN.DISABLED
                  validate:
                    type: bool

                header.cache_enable:
                  type: toggle
                  toggleable: true
                  label: PLUGIN_ADMIN.CACHING
                  highlight: 1
                  options:
                    1: PLUGIN_ADMIN.ENABLED
                    0: PLUGIN_ADMIN.DISABLED
                  validate:
                    type: bool

                header.visible:
                  type: toggle
                  toggleable: true
                  label: PLUGIN_ADMIN.VISIBLE
                  help: PLUGIN_ADMIN.VISIBLE_HELP
                  highlight: 1
                  options:
                    1: PLUGIN_ADMIN.ENABLED
                    0: PLUGIN_ADMIN.DISABLED
                  validate:
                    type: bool

                header.template:
                  type: text
                  toggleable: true
                  label: PLUGIN_ADMIN.DISPLAY_TEMPLATE

                header.append_url_extension:
                  type: text
                  label: PLUGIN_ADMIN.APPEND_URL_EXT
                  toggleable: true
                  help: PLUGIN_ADMIN.APPEND_URL_EXT_HELP

                header.order_by:
                  type: hidden

                header.order_manual:
                  type: hidden
                  validate:
                    type: commalist

                blueprint:
                  type: blueprint
